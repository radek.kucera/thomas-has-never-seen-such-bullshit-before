﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Controllers.ControllerModels
{
    public class SquadRegister
    {
        public int id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Place { get; set; }
        public string Sport { get; set; }
        public List<string> Users { get; set; }
    }
}

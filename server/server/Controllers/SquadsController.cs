﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using server.Controllers.ControllerModels;
using server.Models;

namespace server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SquadsController : ControllerBase
    {
        private readonly squadDBContext _context;

        public SquadsController()
        {
            _context = new squadDBContext();
        }

        //Squads
       [HttpGet]
        public async Task<ActionResult<IEnumerable<Squads>>> GetSquads()
        {
            return await _context.Squads.Include(sq => sq.Students).ToListAsync();
        }

        //Squads/getsquadusers/5
        [HttpGet("getsquadstudents")]
        public async Task<IEnumerable<Students>> GetSquadStudents(int id)
        {
            return await _context.Students.Where(su => su.SquadId == id).ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Squads>> PostSquads(SquadRegister squads)
        {
            var squad = new Squads
            {
                Id = squads.id,
                Name = squads.Name,
                Sport = squads.Sport,
                Date = squads.Date,
                Place = squads.Place
            };
            _context.Squads.Add(squad);
            try
            {
                var x = squad.Id;
                _context.SaveChanges();
                squads.Users.ForEach(user =>
                {
                    //for each user id add user for this squad
                    _context.Students.Add(new Students { SquadId = squad.Id, Arrived = false, UserName = user});
                });
            }
            catch (DbUpdateException)
            {
                if (SquadsExists(squad.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetSquads", new { id = squad.Id }, squad);
        }

        private bool SquadsExists(int id)
        {
            return _context.Squads.Any(e => e.Id == id);
        }
    }
}

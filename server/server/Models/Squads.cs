﻿using System;
using System.Collections.Generic;

namespace server.Models
{
    public partial class Squads
    {
        public Squads()
        {
            Students = new HashSet<Students>();
        }

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Place { get; set; }
        public string Sport { get; set; }
        public int Id { get; set; }

        public virtual ICollection<Students> Students { get; set; }
    }
}

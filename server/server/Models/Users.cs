﻿using System;
using System.Collections.Generic;

namespace server.Models
{
    public partial class Users
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Id { get; set; }
    }
}

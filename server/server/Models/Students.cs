﻿using System;
using System.Collections.Generic;

namespace server.Models
{
    public partial class Students
    {
        public string UserName { get; set; }
        public int Id { get; set; }
        public int? SquadId { get; set; }
        public bool? Arrived { get; set; }

        public virtual Squads Squad { get; set; }
    }
}

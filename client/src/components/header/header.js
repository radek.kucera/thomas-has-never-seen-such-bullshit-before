function Header() {
  return (
    <header class="header">
      <h2 class="title no-print">SQUAD CREATOR</h2>
      <div class="header-spacer no-print"></div>
    </header>
  );
}

export default Header;

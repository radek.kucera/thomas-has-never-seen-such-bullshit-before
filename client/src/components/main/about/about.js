import aboutInformations from "../../../static/aboutInformations";
import Information from "./information";

function About() {
  const content = aboutInformations.map(item => (
    <Information
      key={"question_" + item.question}
      question={item.question}
      answer={item.answer}
    ></Information>
  ));
  return (
    <section id="main" class="container">
      {content}
    </section>
  );
}

export default About;

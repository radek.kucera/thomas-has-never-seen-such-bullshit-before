function Information(props) {
  return (
    <div class="docs-section">
      <h6 class="docs-header">{props.question}</h6>
      <p>{props.answer}</p>
    </div>
  );
}

export default Information;

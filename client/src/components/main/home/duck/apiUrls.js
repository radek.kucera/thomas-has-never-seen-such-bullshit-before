const apiUrls = {
  test: "https://jsonplaceholder.typicode.com",
  login: "http://10.7.0.52:5001/login",
  squads: "http://10.7.0.52:5001/squads"
};

export default apiUrls;

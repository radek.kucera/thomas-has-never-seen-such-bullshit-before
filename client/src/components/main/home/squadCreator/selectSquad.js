import SelectSquadItem from "./selectSquadItem";
import { Component } from "preact";

class SelectSquad extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 0
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    this.setState({ status: value.target.value });
  }

  render() {
    const items = this.props.data.map(item => {
      return <SelectSquadItem id={item.id} name={item.name}></SelectSquadItem>;
    });

    console.log(this.state.status);

    return (
      <div class="row">
        <select
          onChange={value => {
            this.handleChange(value);
          }}
          class="u-full-width eight columns"
          id="selectBox_Squads"
        >
          {items}
        </select>
        <button
          class="four columns"
          onClick={() => {
            this.props.selectChange(this.state.status);
            this.props.viewChange();
          }}
        >
          CHOOSE
        </button>
      </div>
    );
  }
}

export default SelectSquad;

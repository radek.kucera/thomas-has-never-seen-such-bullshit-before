import { Component } from "preact";

class SquadView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      squad: null
    };
  }

  componentWillMount() {
    this.props.data.forEach(squad => {
      if (squad.id == this.props.selected) {
        this.setState({ squad: squad });
      }
    });
  }

  render() {
    console.log(this.state.squad);
    const squad = this.state.squad;
    let i = 0;
    const squadMembers = this.state.squad.students.map(students => {
      i++;
      return (
        <tr key={"row_" + i}>
          <td>{i}</td>
          <td>{students.userName}</td>
        </tr>
      );
    });

    return (
      <section>
        <h3>{squad.name}</h3>
        <div class="row">
          <strong>Místo konání: </strong> <span>{squad.place}</span>
        </div>
        <div class="row">
          <strong>Datum konání: </strong> <span>{squad.date}</span>
        </div>
        <div class="row">
          <strong>Sportovní kategoie: </strong>
          <span>{squad.sport}</span>
        </div>
        <div class="row">
          <table class="u-full-width">
            <thead>
              <tr key="row_squad">
                <th>Pozice</th>
                <th>Jméno studenta</th>
              </tr>
            </thead>
            <tbody>{squadMembers}</tbody>
          </table>
        </div>
      </section>
    );
  }
}

export default SquadView;

function selectSquadItem(props) {
  return <option value={props.id}>{props.name}</option>;
}

export default selectSquadItem;

import { Component } from "preact";
import urls from "../duck/apiUrls";
import Axios from "axios";

class CreateSquadNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      date: "",
      place: "",
      sport: "",
      users: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    Axios.post(urls.squads, this.state, {
      headers: { Authorization: this.props.token }
    });
  }

  render() {
    let i = 0;
    const users = this.state.users.map(user => {
      console.log(user);
      i++;
      return (
        <tr key={"row_users_" + i}>
          <td>{i}</td>
          <td>
            <input
              class="u-full-width"
              placeholder="Pepik Frantik"
              component="input"
              type="text"
              onChange={e => {
                this.state.users[this.state.users.indexOf(user)] =
                  e.target.value;
              }}
            ></input>
          </td>
        </tr>
      );
    });

    return (
      <form
        onSubmit={() => {
          this.handleSubmit();
        }}
      >
        <div class="row form-row">
          <div class="six columns">
            <label for="name">Název soupisky: </label>
            <input
              name="name"
              class="u-full-width"
              placeholder="Basketball žáků - Květen 2018"
              component="input"
              type="text"
              onChange={e => {
                this.setState({ name: e.target.value });
              }}
            />
          </div>
          <div class="six columns">
            <label for="date">Datum konání: </label>
            <input
              class="u-full-width"
              type="date"
              name="date"
              component="input"
              type="date"
              onChange={e => {
                this.setState({ date: e.target.value });
              }}
            />
          </div>
        </div>
        <div class="row form-row">
          <div class="six columns">
            <label for="place">Místo konání: </label>
            <input
              class="u-full-width"
              placeholder="Liberec - Přehrada"
              name="place"
              component="input"
              type="text"
              onChange={e => {
                this.setState({ place: e.target.value });
              }}
            />
          </div>
          <div class="six columns">
            <label for="sport">Sport: </label>
            <input
              class="u-full-width"
              placeholder="Basketball"
              name="sport"
              component="input"
              type="text"
              onChange={e => {
                this.setState({ sport: e.target.value });
              }}
            />
          </div>
        </div>
        <div class="row">
          <table class="u-full-width">
            <thead>
              <tr key="row_squad">
                <th>Pozice</th>
                <th>Jméno studenta</th>
              </tr>
            </thead>
            <tbody>{users}</tbody>
          </table>
        </div>
        <div class="row">
          <button
            type="button"
            onClick={() => {
              this.state.users.push("");
              console.log(this.state);
              this.forceUpdate();
            }}
          >
            Přidat studenta
          </button>
        </div>
        <div class="row form-row">
          <button type="submit" class="button-primary">
            Odeslat
          </button>
        </div>
      </form>
    );
  }
}

export default CreateSquadNew;

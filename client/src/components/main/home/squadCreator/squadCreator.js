import { Component } from "preact";
import SelectSquad from "./selectSquad";
import CreateSquad from "./createSquad";
import urls from "../duck/apiUrls";
import SquadView from "./squadView";
import axios from "axios";
import CreateSquadNew from "./createSquadNew";

class SquadCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 0,
      selected: null,
      data: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleChange(view) {
    this.setState({ view: view });
  }

  handleSelect(squadId) {
    this.setState({ selected: squadId });
  }

  render() {
    if (typeof this.props.token == "string" && this.state.data.length == 0) {
      axios
        .get(urls.squads, {
          headers: { Authorization: this.props.token }
        })
        .then(res => {
          this.setState({ data: res.data });
        });
    }

    console.log(this.state);
    console.log(this.props.token);
    if (this.state.view === 0) {
      return (
        <section id="main" class="container docs-section">
          <h3>Get started with your squad!</h3>
          <div class="squadSelector">
            <div class="row">
              <strong>Choose created squads:</strong>
            </div>
            <SelectSquad
              data={this.state.data}
              token={this.props.token}
              selectChange={this.handleSelect}
              viewChange={() => {
                this.handleChange(1);
              }}
            ></SelectSquad>
            <div class="row">
              <strong>Or:</strong>
            </div>
            <CreateSquad
              token={this.props.token}
              viewChange={() => {
                this.handleChange(2);
              }}
            ></CreateSquad>
          </div>
        </section>
      );
    } else if (!this.props.token) {
      return (
        <section id="main" class="container docs-section">
          <h3>Waiting for authorizatoion</h3>
        </section>
      );
    } else if (this.state.view === 1) {
      return (
        <SquadView
          data={this.state.data}
          token={this.props.token}
          selected={this.state.selected}
        ></SquadView>
      );
    } else if (this.state.view === 2) {
      return <CreateSquadNew token={this.props.token}></CreateSquadNew>;
    } else {
      return (
        <section id="main" class="container docs-section">
          <h3>Internal error! Please refresh the page!</h3>
        </section>
      );
    }
  }
}

export default SquadCreator;

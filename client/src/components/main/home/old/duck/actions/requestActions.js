import types from "../types/requestTypes";
import axios from "axios";
import urls from "../constants/apiUrls";

const requestActionCreators = {
  getLogin: (username, password) => dispatch => {
    dispatch({ type: types.REQUEST_START });
    axios
      .post(urls.login, { name: username, password: password })
      .then(res => {
        dispatch({ type: types.REQUEST_RESOLVE, payload: res.data });
      })
      .catch(error => {
        console.log(urls.login, error);
        dispatch({ type: types.REQUEST_FAILURE, payload: error });
      });
  },
  testRequest: () => dispatch => {
    dispatch({ type: types.REQUEST_START });
    axios
      .get(urls.test, {})
      .then(res => {
        dispatch({ type: types.REQUEST_RESOLVE, payload: res.data });
      })
      .catch(error => {
        dispatch({ type: types.REQUEST_FAILURE, payload: error.response });
      });
  },
  getSquads: token => dispatch => {
    dispatch({ type: types.REQUEST_START });
    axios
      .get(
        urls.squads,
        {},
        {
          headers: { Authorization: "Bearer " + token }
        }
      )
      .then(res => {
        dispatch({ type: types.REQUEST_RESOLVE, payload: res.data });
      })
      .catch(error => {
        console.log(urls.login, error);
        dispatch({ type: types.REQUEST_FAILURE, payload: error });
      });
  }
};
export default requestActionCreators;

import { combineReducers } from "redux";
import request from "./requestReducers";
import squads from "./squadsReducers";
import { reducer as formReducer } from "redux-form";

const rootReducer = () => {
  return combineReducers({
    request: request,
    squads: squads,
    form: formReducer
  });
};

export default rootReducer;

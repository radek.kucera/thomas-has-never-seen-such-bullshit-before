import initialState from "../constants/requestInitialState";
import types from "../types/requestTypes";

const squadReducer = (state, action) => {
  state = state || initialState;

  switch (action.type) {
    case types.REQUEST_START: {
      return { ...initialState, busy: true };
    }
    case types.REQUEST_FAILURE: {
      return { ...initialState, error: true };
    }
    case types.REQUEST_RESOLVE: {
      return { ...initialState, response: action.payload };
    }
    default: {
      return state;
    }
  }
};

export default squadReducer;

const requestInitialState = {
  error: false,
  busy: false,
  response: null
};

export default requestInitialState;

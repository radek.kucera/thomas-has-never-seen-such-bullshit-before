import SelectBoxItem from "./selectBoxItem";
import { Component } from "preact";
import { connect } from "react-redux";
import axios from "axios";

class SelectBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  async componentDidMount() {
    console.log(this.props.request);
    const res = await axios.get("https://jsonplaceholder.typicode.com/todos");
    this.setState({ data: res.data.slice(0, 10) });
  }

  render() {
    const squads = this.state.data.map(squad => {
      return <SelectBoxItem id={squad.id} name={squad.title}></SelectBoxItem>;
    });

    return (
      <div class="squadSelector">
        <label for="selectBox_Squads">Choose created squads:</label>
        <div class="row">
          <select class="u-full-width eight columns" id="selectBox_Squads">
            {squads}
          </select>
          <button class="four columns">CHOOSE</button>
        </div>
        <div class="row">
          <strong>Or:</strong>
        </div>
        <div class="row">
          <button class="button-primary">Create new one!</button>
        </div>
      </div>
    );
  }
}

export default connect(state => {
  return {
    request: state.request
  };
}, {})(SelectBox);

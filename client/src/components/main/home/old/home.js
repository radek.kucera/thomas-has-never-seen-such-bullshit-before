import { Provider } from "react-redux";
import configureStore from "./duck/configureStore";
import Login from "./login";
import SquadCreator from "./squadCreator";

const initialState = {};

const store = configureStore(initialState);

function Home() {
  return (
    <Provider store={store}>
      <Login></Login>
      <SquadCreator></SquadCreator>
    </Provider>
  );
}

export default Home;

import { connect } from "react-redux";
import { useState, useEffect } from "preact/hooks";
import requestAc from "./duck/actions/requestActions";
import { Component } from "preact";
import SelectBox from "./selectBox";

class Login extends Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    await this.props.getLogin("admin", "admin");
  }

  render() {
    console.log(this.props.request);
    return;
  }
}

export default connect(
  state => {
    return {
      request: state.request,
      form: state.form
    };
  },
  {
    getLogin: requestAc.getLogin,
    testRequest: requestAc.testRequest
  }
)(Login);

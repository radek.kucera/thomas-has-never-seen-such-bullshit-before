import { Field, reduxForm, FieldArray } from "redux-form";
import { Component } from "preact";

function NewSquad(props) {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div class="row form-row">
        <div class="six columns">
          <label for="name">Název soupisky: </label>
          <Field
            name="name"
            class="u-full-width"
            placeholder="Basketball žáků - Květen 2018"
            component="input"
            type="text"
          />
        </div>
        <div class="six columns">
          <label for="date">Datum konání: </label>
          <Field
            class="u-full-width"
            type="date"
            name="date"
            component="input"
            type="date"
          />
        </div>
      </div>
      <div class="row form-row">
        <div class="six columns">
          <label for="place">Místo konání: </label>
          <Field
            class="u-full-width"
            placeholder="Liberec - Přehrada"
            name="place"
            component="input"
            type="text"
          />
        </div>
        <div class="six columns">
          <label for="sport">Sport: </label>
          <Field
            class="u-full-width"
            placeholder="Basketball"
            name="sport"
            component="input"
            type="text"
          />
        </div>
      </div>
      <div class="row">
        <FieldArray name="members" component={RenderMembers} />
      </div>
      <div class="row form-row">
        <button type="submit" class="button-primary">
          Odeslat
        </button>
      </div>
    </form>
  );
}

class RenderMembers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      fields: []
    };
  }

  render() {
    const users = this.state.fields.map(index => {
      return (
        <tr>
          <td>{index + 1}</td>
          <td>Hello world!</td>
        </tr>
      );
    });

    return (
      <>
        <button
          type="button"
          onClick={() => {
            this.setState({ index: this.state.index + 1 });
            this.state.fields.push(this.state.index);
            console.log(this.state.fields);
          }}
        >
          Přidat sportovce
        </button>
        <table class="u-full-width">{users}</table>
      </>
    );
  }
}

export default reduxForm({
  form: "squad"
})(NewSquad);

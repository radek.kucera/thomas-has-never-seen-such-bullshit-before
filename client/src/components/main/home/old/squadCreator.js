import SelectBox from "./selectBox";
import NewSquad from "./newSquad";
import { connect } from "react-redux";
import requestAc from "./duck/actions/requestActions";
import { Component } from "preact";
import axios from "axios";

function SquadCreator(props) {
  return (
    <section id="main" class="container docs-section">
      <h3>Get started with your squad!</h3>
      {/*<IsAuthorized status={props.request.response}></IsAuthorized>*/}
      <SQC token="token"></SQC>
    </section>
  );
}

function IsAuthorized(props) {
  if (props.status) {
    return <SQC token={props.status.token}></SQC>;
  } else return <h4>Waiting for authorization...</h4>;
}

class SQC extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  async handleSubmit() {
    console.log("Bearer " + this.props.token);
    const res = await axios.get("https://jsonplaceholder.typicode.com/todos/1");
    this.setState({ data: res.data });
  }

  render() {
    console.log(this.state.data);
    return (
      <>
        <SelectBox></SelectBox>
        <NewSquad
          onSubmit={values => {
            console.log(values);
          }}
        ></NewSquad>
      </>
    );
  }
}

export default connect(
  state => {
    return {
      squads: state.squads,
      request: state.request
    };
  },
  {
    getSquads: requestAc.getSquads
  }
)(SquadCreator);

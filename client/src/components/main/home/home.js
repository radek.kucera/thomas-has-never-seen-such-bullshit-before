import Axios from "axios";
import urls from "./duck/apiUrls";
import { Component } from "preact";
import SquadCreator from "./squadCreator/squadCreator";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { token: null };
  }

  async componentDidMount() {
    const res = await Axios.post(urls.login, {
      name: "admin",
      password: "admin"
    });

    if (res.data.token) {
      this.setState({ token: "Bearer " + res.data.token });
    }

    //this.setState({ token: "Bearer " + "token" });
  }

  render() {
    return <SquadCreator token={this.state.token}></SquadCreator>;
  }
}

export default Home;

import Router from "preact-router";
import AsyncRoute from "preact-async-route";
import Home from "./home/home";

function Main() {
  return (
    <main>
      <Router>
        <Home path="/"></Home>
        <AsyncRoute
          path="/about"
          getComponent={() =>
            import("./about/about").then(module => module.default)
          }
        ></AsyncRoute>
        <AsyncRoute
          path="/contact"
          getComponent={() =>
            import("./contact/contact").then(module => module.default)
          }
        ></AsyncRoute>
      </Router>
    </main>
  );
}

export default Main;
